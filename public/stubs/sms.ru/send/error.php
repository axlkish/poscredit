<?php

echo json_encode(
    [
        'status' => 'OK',
        'status_code' => 100,
        'sms' => [
            $_POST['to'] => [
                'status' => 'ERROR',
                'status_code' => 207,
                'status_text' => 'На этот номер (или один из номеров) нельзя отправлять сообщения, либо указано более 100 номеров в списке получателей'
            ]
        ]
    ]
);

