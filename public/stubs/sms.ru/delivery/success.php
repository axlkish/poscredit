<?php

echo json_encode(
    [
        'status' => 'OK',
        'status_code' => 100,
        'sms' => [
            $_POST['sms_id'] => [
                'status' => 'OK',
                'status_code' => 103,
                'cost' => 0.50,
                'status_text' => 'Сообщение доставлено'
            ]
        ]
    ]
);

