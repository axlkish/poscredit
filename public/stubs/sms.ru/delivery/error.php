<?php

echo json_encode(
    [
        'status' => 'OK',
        'status_code' => 100,
        'sms' => [
            $_POST['sms_id'] => [
                'status' => 'ERROR',
                'status_code' => 105,
                'status_text' => 'Не может быть доставлено: удалено оператором'
            ]
        ]
    ]
);

