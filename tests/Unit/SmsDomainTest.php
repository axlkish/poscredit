<?php

namespace App\Tests\Unit;

use App\Domain\SmsDomain;
use App\Forms\SmsSendForm;
use App\Services\Http\Sms\SmsService;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SmsDomainTest extends TypeTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->request = $this->getRequestMock();

        $this->form = $this->getSendSmsForm();

        $this->smsService =
            $this->getMockBuilder(SmsService::class)
                ->setConstructorArgs([
                    $this->createMock(LoggerInterface::class),
                    $this->createMock(HttpClientInterface::class)

                ])
                ->onlyMethods(['send'])
                ->getMock();

        $this->smsDomain =
            $this->getMockBuilder(SmsDomain::class)
                ->setConstructorArgs([
                    $this->smsService,
                    $this->createMock(ContainerBagInterface::class),
                    $this->createMock(ManagerRegistry::class),
                    $this->createMock(TranslatorInterface::class)
                ])
                ->onlyMethods(['handleForm', 'getSendParams', 'getSendUrl', 'addSms'])
                ->getMock();
    }

    public function testIsSendSuccess(): void
    {
        $this->smsDomain->method('handleForm')
            ->willReturn($this->form);

        $this->smsDomain->method('getSendUrl')
            ->willReturn('');

        $this->smsDomain->method('getSendParams')
            ->willReturn([]);

        $this->smsService->method('send')
            ->willReturn(true);

        $this->smsDomain->method('addSms')
            ->willReturn(true);

        $result = $this->smsDomain->isSend($this->request);

        $this->assertTrue($result);
    }

    public function testIsSendError(): void
    {
        $this->smsDomain->method('handleForm')
            ->willReturn($this->form);

        $this->smsDomain->method('getSendParams')
            ->willReturn([]);

        $this->smsDomain->method('getSendUrl')
            ->willReturn('');

        $this->smsService->method('send')
            ->willReturn(false);

        $this->smsDomain->method('addSms')
            ->willReturn(false);

        $result = $this->smsDomain->isSend($this->request);

        $this->assertFalse($result);
    }

    private function getRequestMock(): Request
    {
        $inputBag = new InputBag();
        $inputBag->set('phone', '79995556644');
        $requestMock = $this->createMock(Request::class);
        $requestMock->request = $inputBag;

        return $requestMock;
    }

    private function getSendSmsForm(): FormInterface
    {
        $formData = ['phone' => '79995556644'];
        $form = $this->factory->create(SmsSendForm::class);
        $form->submit($formData);

        return $form;
    }
}
