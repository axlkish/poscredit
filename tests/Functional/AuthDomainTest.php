<?php

namespace App\Tests\Functional;

use App\Domain\AuthDomain;
use App\Domain\SmsDomain;
use App\Entity\SmsEntity;
use App\Enums\SmsStatusCodeEnum;
use App\Forms\AuthForm;
use App\Repository\SmsRepository;
use App\Services\Http\Sms\SmsService;
use Carbon\Carbon;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @group functional
 */
class AuthDomainTest extends TypeTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->sms = $this->getSmsEntity();
        $this->managerRegistry = $this->getManagerRegistryMock();
        $this->request = $this->getRequestMock();

        $this->authDomain =
            $this->getMockBuilder(AuthDomain::class)
                ->setConstructorArgs([
                    $this->managerRegistry
                ])
                ->onlyMethods(['handleForm'])
                ->getMock();

        $this->smsService =
            $this->getMockBuilder(SmsService::class)
                ->setConstructorArgs([
                    $this->createMock(LoggerInterface::class),
                    $this->createMock(HttpClientInterface::class)

                ])
                ->onlyMethods(['send'])
                ->getMock();

        $this->smsService->result = $this->getSmsServiceSuccessResult();

        $this->smsDomain =
            $this->getMockBuilder(SmsDomain::class)
                ->setConstructorArgs([
                    $this->smsService,
                    $this->createMock(ContainerBagInterface::class),
                    $this->managerRegistry,
                    $this->createMock(TranslatorInterface::class)
                ])
                ->onlyMethods(['getCheckStatusUrl', 'getCheckStatusParams'])
                ->getMock();
    }

    public function testIsAuthByPhoneAndSmsCodeSuccess()
    {
        $this->authDomain->method('handleForm')
            ->willReturn($this->getAuthFormSuccess());

        $this->smsDomain->method('getCheckStatusUrl')
            ->willReturn('');

        $this->smsDomain->method('getCheckStatusParams')
            ->willReturn([]);

        $this->smsService->method('send')
            ->willReturn(true);

        $this->smsDomain->checkStatus('000000-10000000');
        $this->assertEquals(SmsStatusCodeEnum::DELIVERY_OK, $this->sms->getStatusCode());

        $result = $this->authDomain->isAuthByPhoneAndSmsCode($this->request);
        $this->assertTrue($result);
    }

    public function testIsAuthByPhoneAndSmsCodeError()
    {
        $this->authDomain->method('handleForm')
            ->willReturn($this->getAuthFormError());

        $this->smsDomain->method('getCheckStatusUrl')
            ->willReturn('');

        $this->smsDomain->method('getCheckStatusParams')
            ->willReturn([]);

        $this->smsService->method('send')
            ->willReturn(true);

        $this->smsDomain->checkStatus('000000-10000000');
        $this->assertEquals(SmsStatusCodeEnum::DELIVERY_OK, $this->sms->getStatusCode());

        $result = $this->authDomain->isAuthByPhoneAndSmsCode($this->request);
        $this->assertFalse($result);
    }

    private function getSmsEntity(): SmsEntity
    {
        return
            (new SmsEntity())
                ->setPhone('79995556644')
                ->setCode('5555')
                ->setStatus('OK')
                ->setStatusCode(100)
                ->setSmsId('000000-10000000')
                ->setCreatedAt(Carbon::now())
        ;
    }

    private function getManagerRegistryMock(): ManagerRegistry
    {
        $smsRepository = $this->createMock(SmsRepository::class);

        $smsRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($this->sms);

        $managerRegistry = $this->createMock(managerRegistry::class);
        $managerRegistry->expects($this->any())
            ->method('getRepository')
            ->willReturn($smsRepository);

        $managerRegistry->expects($this->any())
            ->method('getManager')
            ->willReturn($this->createMock(ObjectManager::class));

        return $managerRegistry;
    }

    private function getRequestMock(): Request
    {
        $inputBag = new InputBag();
        $inputBag->set('phone', '79995556644');
        $inputBag->set('sms_code', '5555');
        $requestMock = $this->createMock(Request::class);
        $requestMock->request = $inputBag;

        return $requestMock;
    }

    private function getAuthFormSuccess(): FormInterface
    {
        $formData = [
            'phone' => '79995556644',
            'sms_code' => '5555'
        ];
        $form = $this->factory->create(AuthForm::class);
        $form->submit($formData);

        return $form;
    }

    private function getAuthFormError(): FormInterface
    {
        $formData = [
            'phone' => '79995556644',
            'sms_code' => '3333'
        ];
        $form = $this->factory->create(AuthForm::class);
        $form->submit($formData);

        return $form;
    }

    private function getSmsServiceSuccessResult(): array
    {
        return
        [
            'status' => 'OK',
            'status_code' => 100,
            'sms' => [
                '000000-10000000' => [
                    'status' => 'OK',
                    'status_code' => 103,
                    'cost' => 0.50,
                    'status_text' => 'Сообщение доставлено'
                ]
            ]
        ];
    }
}
