<?php

namespace App\Controller\Api;

use App\Domain\AuthDomain;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Domain\SmsDomain;

class ApiV1Controller extends AbstractController
{
    const TRANS_SMS_SEND_SUCCESS = 'sms.send.code.success';
    const TRANS_SMS_SEND_ERROR = 'sms.send.code.error';

    const TRANS_AUTH_ACCESS_SUCCESS = 'auth.access.success';
    const TRANS_AUTH_ACCESS_FORBIDDEN = 'auth.access.forbidden';
    const TRANS_AUTH_ERROR = 'auth.error';

    /**
     * @var TranslatorInterface
     */
    public TranslatorInterface $translator;

    /**
     * @var SmsDomain
     */
    public SmsDomain $smsDomain;

    /**
     * @var AuthDomain
     */
    public AuthDomain $authDomain;

    public function __construct(
        TranslatorInterface $translator,
        SmsDomain $smsDomain,
        AuthDomain $authDomain
    ) {
        $this->translator = $translator;
        $this->smsDomain = $smsDomain;
        $this->authDomain = $authDomain;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function sendSmsCode(Request $request): JsonResponse
    {
        if ($this->smsDomain->isSend($request)) {
            return $this->json(
                ['message' => $this->translator->trans(self::TRANS_SMS_SEND_SUCCESS)],
                Response::HTTP_OK
            );
        }

        return $this->getErrorsResponse(
            $this->translator->trans(self::TRANS_SMS_SEND_ERROR),
            $this->smsDomain->errors
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function authByPhoneAndSmsCode(Request $request): JsonResponse
    {
        if ($this->authDomain->isAuthByPhoneAndSmsCode($request)) {
            return $this->json(
                ['message' => $this->translator->trans(self::TRANS_AUTH_ACCESS_SUCCESS)],
                Response::HTTP_OK
            );
        } elseif (!empty($this->authDomain->errors)) {
            return $this->getErrorsResponse(
                $this->translator->trans(self::TRANS_AUTH_ERROR),
                $this->authDomain->errors
            );
        } else {
            return $this->json(
                ['message' => $this->translator->trans(self::TRANS_AUTH_ACCESS_FORBIDDEN)],
                Response::HTTP_FORBIDDEN
            );
        }
    }

    /**
     * @param string $message
     * @param array $errors
     * @return JsonResponse
     */
    private function getErrorsResponse(string $message, array $errors): JsonResponse
    {
        return $this->json(
            [
                'message' => $message,
                'errors' => $errors
            ],
            Response::HTTP_BAD_REQUEST
        );
    }
}
