<?php

namespace App\Services\Http\Sms;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;

class SmsService
{
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;

    /**
     * @var string
     */
    public string $url;

    /**
     * @var array
     */
    public array $params = [];

    /**
     * @var array
     */
    public array $result = [];

    /**
     * @var array
     */
    public array $errors = [];

    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $client;

    public function __construct(
        LoggerInterface $logger,
        HttpClientInterface $client
    ) {
        $this->logger = $logger;
        $this->client = $client;
    }

    /**
     * @return bool
     */
    public function send(string $url, array $params): bool
    {
        $this->url = $url;
        $this->params = $params;

        $formData = new FormDataPart($this->params);

        try {
            $response = $this->client->request(
                Request::METHOD_POST,
                $this->url,
                [
                    'headers' => $formData->getPreparedHeaders()->toArray(),
                    'body' => $formData->bodyToIterable(),
                ]
            );

            $this->result = json_decode($response->getContent(), true);
            return true;
        } catch (\Throwable $e) {
            $this->errors = ['message' => $e->getMessage()];
            $this->logger->error($e->getMessage(), ['url' => $this->url, 'params' => $this->params]);
            return false;
        }
    }
}