<?php

namespace App\Domain;

use App\Entity\SmsEntity;
use App\Helpers\SmsHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;
use App\Dto\SmsSendDto;
use App\Forms\SmsSendForm;
use App\Helpers\FormHelper;
use App\Services\Http\Sms\SmsService;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Carbon\Carbon;
use Symfony\Contracts\Translation\TranslatorInterface;

class SmsDomain extends AbstractController
{
    const SMS_RU_SEND_URL = 'sms.ru.send.url';
    const SMS_RU_API_ID = 'sms.ru.api.id';
    const SMS_RU_CHECK_STATUS_URL = 'sms.ru.check.status.url';

    const TRANS_SMS_SEND_ALREADY = 'sms.send.code.already';

    /**
     * @var SmsService
     */
    public SmsService $smsService;

    /**
     * @var ContainerBagInterface
     */
    public ContainerBagInterface $params;

    /**
     * @var ManagerRegistry
     */
    public ManagerRegistry $doctrine;

    /**
     * @var TranslatorInterface
     */
    public TranslatorInterface $translator;

    /**
     * @var array
     */
    public array $errors = [];

    public function __construct(
        SmsService $smsService,
        ContainerBagInterface $params,
        ManagerRegistry $doctrine,
        TranslatorInterface $translator
    ) {
        $this->smsService = $smsService;
        $this->params = $params;
        $this->doctrine = $doctrine;
        $this->translator = $translator;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function isSend(Request $request): bool
    {
        $form = $this->handleForm($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($this->smsService->send(
                $this->getSendUrl(),
                $this->getSendParams($form)
            )) {
                return $this->processSendSmsResult();
            } else {
                $this->errors = $this->smsService->errors;
                return false;
            }
        }

        $this->errors = FormHelper::getErrors($form);
        return false;
    }

    /**
     * @return bool
     */
    public function checkStatus(string $smsId): bool
    {
        if ($this->smsService->send(
            $this->getCheckStatusUrl(),
            $this->getCheckStatusParams($smsId)
        )) {
            return $this->processSmsCheckStatusResult($smsId);
        } else {
            $this->errors = $this->smsService->errors;
            return false;
        }

    }

    /**
     * @return bool
     */
    protected function processSendSmsResult(): bool
    {
        if (empty($this->smsService->errors)) {
            return $this->addSms();
        } else {
            $this->errors = $this->smsService->errors;
            return false;
        }
    }

    /**
     * @return bool
     */
    protected function addSms(): bool
    {
        $phone = $this->smsService->params['to'];
        $code = $this->smsService->params['msg'];
        $phoneResult = $this->smsService->result['sms'][$phone];

        $sms = $this->doctrine->getRepository(SmsEntity::class)->findOneBy(['sms_id' => $phoneResult['sms_id']]);

        if (!is_null($sms)) {
            $this->errors = ['message' => $this->translator->trans(self::TRANS_SMS_SEND_ALREADY)];
            return false;
        } else {
            $storeSms = (new SmsEntity())
                ->setPhone($phone)
                ->setCode($code)
                ->setStatus($phoneResult['status'])
                ->setStatusCode($phoneResult['status_code'])
                ->setSmsId($phoneResult['sms_id'])
                ->setCreatedAt(Carbon::now())
                ->setUpdatedAt(Carbon::now())
            ;

            $em = $this->doctrine->getManager();
            $em->persist($storeSms);
            $em->flush();
        }

        return true;
    }

    /**
     * @param Request $request
     * @return FormInterface
     */
    protected function handleForm(Request $request): FormInterface
    {
        return
            $this->createForm(SmsSendForm::class, new SmsSendDto())
                ->submit($request->request->all());
    }

    /**
     * @return mixed
     */
    protected function getSendUrl(): mixed
    {
        return $this->params->get(self::SMS_RU_SEND_URL);
    }

    /**
     * @param FormInterface $form
     * @return array
     */
    protected function getSendParams(FormInterface $form): array
    {
        return [
            'api_id' => $this->params->get(self::SMS_RU_API_ID),
            'to' => $form->get('phone')->getData(),
            'msg' => SmsHelper::getCode(),
            'json' => '1'
        ];
    }

    /**
     * @return mixed
     */
    protected function getCheckStatusUrl(): mixed
    {
        return $this->params->get(self::SMS_RU_CHECK_STATUS_URL);
    }

    /**
     * @param string $smsId
     * @return array
     */
    protected function getCheckStatusParams(string $smsId): array
    {
        return [
            'api_id' => $this->params->get(self::SMS_RU_API_ID),
            'sms_id' => $smsId,
            'json' => '1'
        ];
    }

    /**
     * @param string $smsId
     * @return bool
     */
    protected function processSmsCheckStatusResult(string $smsId): bool
    {
        $smsIdResult = $this->smsService->result['sms'][$smsId];

        $sms = $this->doctrine->getRepository(SmsEntity::class)->findOneBy(['sms_id' => $smsId]);

        /** @var SmsEntity $sms */
        $sms
            ->setStatus($smsIdResult['status'])
            ->setStatusCode($smsIdResult['status_code'])
            ->setCost($smsIdResult['cost'])
            ->setStatusText($smsIdResult['status_text'])
            ->setUpdatedAt(Carbon::now())
        ;

        $em = $this->doctrine->getManager();
        $em->flush();

        return true;
    }
}