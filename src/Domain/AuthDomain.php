<?php

namespace App\Domain;

use App\Dto\AuthDto;
use App\Entity\SmsEntity;
use App\Enums\SmsStatusCodeEnum;
use App\Enums\SmsStatusEnum;
use App\Forms\AuthForm;
use App\Helpers\FormHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class AuthDomain extends AbstractController
{
    /**
     * @var ManagerRegistry
     */
    public ManagerRegistry $doctrine;

    /**
     * @var array
     */
    public array $errors = [];

    public function __construct(ManagerRegistry $doctrine) {
        $this->doctrine = $doctrine;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function isAuthByPhoneAndSmsCode(Request $request): bool
    {
        $form = $this->handleForm($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $phone = $form->get('phone')->getData();
            $smsCode = (string) $form->get('sms_code')->getData();

            $sms = $this->doctrine->getRepository(SmsEntity::class)->findOneBy([
                'phone' => $phone,
                'code' => $smsCode
            ]);

            return !is_null($sms) && $this->checkAccessByPhoneAndSmsCode($sms, $smsCode);
        }

        $this->errors = FormHelper::getErrors($form);
        return false;
    }

    /**
     * @param SmsEntity $sms
     * @param string $smsCode
     * @return bool
     */
    public function checkAccessByPhoneAndSmsCode(SmsEntity $sms, string $smsCode): bool
    {
        return
            $sms->getStatusCode() == SmsStatusCodeEnum::getDeliveryOk()
                &&
            $sms->getStatus() === SmsStatusEnum::getOk()
                &&
            $sms->getCode() === $smsCode
        ;
    }

    /**
     * @param Request $request
     * @return FormInterface
     */
    protected function handleForm(Request $request): FormInterface
    {
        return
            $this->createForm(AuthForm::class, new AuthDto())
                ->submit($request->request->all());
    }
}