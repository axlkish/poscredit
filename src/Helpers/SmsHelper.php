<?php

namespace App\Helpers;

class SmsHelper
{
    /**
     * @return string
     * @throws \Exception
     */
    public static function getCode(): string
    {
        return (string) random_int(1000, 9999);
    }
}