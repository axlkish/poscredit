<?php

namespace App\Helpers;

use Symfony\Component\Form\FormInterface;

class FormHelper
{
    /**
     * @param FormInterface $form
     * @return array
     */
    public static function getErrors(FormInterface $form): array
    {
        $errors = [];

        foreach ($form->getErrors(true) as $error) {
            if ($error->getOrigin()) {
                $errors[$error->getOrigin()->getName()][] = $error->getMessage();
            }
        }
        return $errors;
    }
}