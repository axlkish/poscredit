<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class SmsSendDto
{
    #[Assert\NotBlank()]
    #[Assert\Regex("/^(7+\d{10})$/")]
    public string $phone;
}
