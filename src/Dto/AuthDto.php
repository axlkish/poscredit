<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class AuthDto
{
    #[Assert\NotBlank()]
    #[Assert\Regex("/^(7+\d{10})$/")]
    public string $phone;


    #[Assert\NotBlank()]
    #[Assert\Range(min: 1000, max: 9999)]
    public int $sms_code;
}
