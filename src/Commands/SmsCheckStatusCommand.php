<?php

namespace App\Commands;

use App\Domain\SmsDomain;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'sms:check-status',
    description: 'Check and update sms status code.',
    hidden: false
)]
class SmsCheckStatusCommand extends Command
{
    /**
     * @var SmsDomain
     */
    private SmsDomain $smsDomain;

    public function __construct(SmsDomain $smsDomain)
    {
        parent::__construct();

        $this->smsDomain = $smsDomain;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('sms_id', InputArgument::REQUIRED, 'sms id to check')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->smsDomain->checkStatus($input->getArgument('sms_id'));
        return Command::SUCCESS;
    }
}
