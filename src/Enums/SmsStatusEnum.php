<?php

namespace App\Enums;

class SmsStatusEnum
{
    private const OK = 'OK';
    private const ERROR = 'ERROR';

    /**
     * @return string
     */
    public static function getOk(): string
    {
        return static::OK;
    }

    /**
     * @return string
     */
    public static function getError(): string
    {
        return static::ERROR;
    }
}
