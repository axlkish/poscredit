<?php

namespace App\Enums;

class SmsStatusCodeEnum
{
    const SEND_OK = 100;
    const DELIVERY_OK = 103;

    /**
     * @return string
     */
    public static function getSendOk(): string
    {
        return static::SEND_OK;
    }

    /**
     * @return string
     */
    public static function getDeliveryOk(): string
    {
        return static::DELIVERY_OK;
    }
}
