<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221008204255 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'create sms table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            CREATE TABLE sms (
                id SERIAL NOT NULL,
                phone VARCHAR(255) NUll,
                code  VARCHAR(16) NUll,
                status VARCHAR(255) NUll,
                status_code INTEGER NUll,
                sms_id VARCHAR(255) NUll,
                cost NUMERIC(9,2) NUll,
                status_text TEXT NUll,
                created_at TIMESTAMP NOT NULL,
                updated_at TIMESTAMP NOT NULL,
                PRIMARY KEY(id)
            );
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE sms;');
    }
}
